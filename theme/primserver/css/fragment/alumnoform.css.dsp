.form-alum{
	
}

.form-alum * { 
	font-size: 14px !important;
	font-family: verdana !important;
} 

.form-alum .z-row:hover { 
	background: #ffffff !important;
}

.form-alum span{
    margin-bottom: 10px;	
} 


.form-alum p{
	overflow: auto;
    margin-bottom: 10px;	
} 

.table td {
    width: 100%;
    display: inline-block;
    white-space: nowrap;
}

.docenteTable table {
  table-layout: fixed;
  width: 100% !important;
}

.docenteTable td:nth-child(1),
.docenteTable td:nth-child(2),
.docenteTable td:nth-child(3) {
  width: 30%;
}

.docenteTable th:nth-child(1),
.docenteTable th:nth-child(2),
.docenteTable th:nth-child(3) {
  width: 30%;
}

.acudienteTable table {
  table-layout: fixed;
  width: 100% !important;
}

.acudienteTable td:nth-child(1),
.acudienteTable td:nth-child(2),
.acudienteTable td:nth-child(3) {
  width: 30%;
}

.acudienteTable th:nth-child(1),
.acudienteTable th:nth-child(2),
.acudienteTable th:nth-child(3) {
  width: 30%;
}

.labeldiv {
margin-top:10px;
margin-bottom:10px
}

.labelalumtitle {
    font-size: 20px !important;
}

.layout1_2{
	width:90%;
}

.layout1_2 .z-hlayout-inner{
	width:100%;
}

.layout2_1{
	width:90%;
}

.layout2_1 .z-hlayout-inner{
	width:100%;
}

.selectclass{
	width: 290px !important;
}

.messagebox{
	width: 100%;
	min-height: 200px;
	border: 1px;
	border-color: #cfcfcf;
	border-style: solid;
	overflow: auto;
}

.fontbold
{
	font-weight: bold;
}

.form-reademail{
	font-size: 14px !important;
	font-family: verdana !important;
}

.btnlink {
     background:none!important;
     color:inherit !important;
     border:none !important; 
     padding:0!important;
     font: inherit !important;
     /*border is optional*/
     border-bottom:0px solid #fff !important; 
     cursor: pointer !important;
     font-size: 14px !important;
	 color: blue !important;
	 text-decoration: underline;
}


